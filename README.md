### What is this repository for? ###

* This is a demo app created to test building a simple post application in React and Redux with a little json-server and expressjs thrown in
* Version 1

### How do I get set up? ###

- After unpacking the repo navigate into the app directory and run the following command in a terminal (http://localhost:3000/posts)
```
json-server --watch post.json
```
- Next you will need to start the expressjs endpoint to allow for file uploads start that server with the following command (http://localhost:8080/upload)
```
node src/server/index.js
```
- Run the following commands in a new terminal window to run the app (http://localhost:3001)
```
npm install
```
```
npm start
```

### References

- https://www.npmjs.com/package/express-fileupload
- https://github.com/typicode/json-server
- https://react-md.mlaursen.com/components/dialogs