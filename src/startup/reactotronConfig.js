import Reactotron, { trackGlobalErrors } from 'reactotron-react-js'
import { reactotronRedux } from 'reactotron-redux'
import apisaucePlugin from 'reactotron-apisauce'

Reactotron.configure({ name: 'React Demo' })
  .use(reactotronRedux())
  .use(apisaucePlugin())
  .use(
    trackGlobalErrors({
      offline: true,
    }),
  )
  .connect()
