import { create } from 'apisauce'

const uploadApi = create({
    baseURL: 'http://localhost:8080/upload',
    headers: {
        'Content-Type': 'multipart/form-data'
      }
})

export default uploadApi