const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();
 
// default options 
app.use(fileUpload());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
 
app.post('/upload', function(req, res) {    
    if (!req.files)
        return res.status(400).send('No files were uploaded.');
    
    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file 
    let sampleFile = req.files.image;
    
    // Use the mv() method to place the file somewhere on your server 
    sampleFile.mv(`./src/server/uploads/${req.files.image.name}`, function(err) {
        if (err)
        return res.status(500).send(err);
    
        res.send('File uploaded!');
    });
});

app.get('/upload', function(req, res) {
    res.sendFile(__dirname + `/uploads/${req.query.filename}`);
})

app.listen(8080, function () {
    console.log('Example app listening on port 8080!')
})