import { GO_TO_HOME } from './pageReducer'
import _ from 'lodash'

// type 
const POSTS_FETCHED = "POSTS_FETCHED"

// action creater
export function postsFetched(posts) {
    const postsMap = _.mapKeys(posts, 'id')
    return {
        type: POSTS_FETCHED,
        payload: postsMap
    }
}

// reducer
export default function (state = null, action) {
    switch(action.type) {
        case POSTS_FETCHED:
            return action.payload
        case GO_TO_HOME:
            return null
        default: 
            return state
    }
} 