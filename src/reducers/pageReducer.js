import { NOT_FOUND } from 'redux-first-router'

// action types
export const GO_TO_HOME = "GO_TO_HOME"
export const GO_TO_POST = "GO_TO_POST"
export const GO_TO_NEW_POST = "GO_TO_NEW_POST"
export const GO_TO_EDIT_POST = "GO_TO_EDIT_POST"

// action creaters
export const goToHome = () => ({ type: GO_TO_HOME })
export const goToPost = id => ({
    type: GO_TO_POST,
    payload: { id },
})
export const goToNew = post => ({ type: GO_TO_NEW_POST, payload: { post } })
export const goToEdit = id => ({ 
    type: GO_TO_EDIT_POST,
    payload: { id }
})

// reducer
export const Pages = {
    PostList: "PostList",
    PostDetail: "PostDetail",
    PostNewEdit: 'PostNewEdit',
    NotFound: 'NotFound',
}

export const pages = {
    [GO_TO_HOME]: Pages.PostList,
    [GO_TO_POST]: Pages.PostDetail,
    [GO_TO_NEW_POST]: Pages.PostNewEdit,
    [GO_TO_EDIT_POST]: Pages.PostNewEdit,
    [NOT_FOUND]: Pages.NotFound,
}

export default (state = Pages.PostList, action) => pages[action.type] || state