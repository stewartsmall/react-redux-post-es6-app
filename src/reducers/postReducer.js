import { GO_TO_POST, GO_TO_EDIT_POST, GO_TO_NEW_POST } from './pageReducer'

// type
const FETCH_POST = "FETCH_POST"

// action creator
export function fetchPost (post) {
    return {
        type: FETCH_POST,
        payload: post
    }
}

// reducer
export default function(state = null, action) {
    switch(action.type){
        case FETCH_POST:
            return action.payload
        case GO_TO_POST:
            return null
        case GO_TO_EDIT_POST:
            return null
        case GO_TO_NEW_POST:
            return null
        default:
            return state
    }
}
