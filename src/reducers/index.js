export { default as page } from './pageReducer'
export { default as postsMap } from './postsReducer'
export { default as post } from './postReducer'