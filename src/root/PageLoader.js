import React from 'react'
import { connect } from 'react-redux'
import styled, { keyframes } from 'styled-components'
import { fadeInLeft } from 'react-animations'

import isLoading from '../selectors/isLoading'
import { Pages } from '../reducers/pageReducer'
import { LoadingSpinner } from './Loading'
import * as pages from '../pages'

const pageMap = {
  [Pages.PostList]: pages.PostList,
  [Pages.PostDetail]: pages.PostDetail,
  [Pages.PostNewEdit]: pages.PostNewEdit,
  [Pages.NotFound]: pages.NotFound,
}

const PageSwitcher = ({ page }) => { 
  const Page = pageMap[page]
  return <Page />
}

const AnimatedDiv = styled.div`animation: .5s ${keyframes`${fadeInLeft}`};`

export const PageLoader = ({ page, isPageLoading }) =>
  isPageLoading
    ?  <LoadingSpinner />
    : <AnimatedDiv>
        <PageSwitcher page={page} />
      </AnimatedDiv>

const mapStateToProps = state => ({
  page: state.page,
  isPageLoading: isLoading(state),
})

export default connect(mapStateToProps)(PageLoader)
