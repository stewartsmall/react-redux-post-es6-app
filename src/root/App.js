import React, { Component } from 'react'
import PageLoader from './PageLoader'
import './App.css'

class App extends Component {
    render() {
        return (
            <div className="App">
                <PageLoader />
            </div>
        )
    }
}

export default App