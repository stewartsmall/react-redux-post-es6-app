import React from 'react'
import Spinner from 'react-spinkit'
import styled from 'styled-components'
import { Flex } from 'grid-styled'

const StyledFlex = styled(Flex)`
  height: 100% 
`

const centered = element =>
  <StyledFlex justify="center" align="center">
    {element}
  </StyledFlex>

export const LoadingSpinner = () => centered(<Spinner name="wandering-cubes" />)

export const Loading = props => {
  if (props.isLoading) {
    // While our other component is loading...
    if (props.timedOut) {
      // In case we've timed out loading our other component.
      return centered(<div>Error! Page load timed out</div>)
    } else if (props.pastDelay) {
      // Display a loading screen after a set delay.
      return <LoadingSpinner />
    } else {
      // Don't flash "Loading..." when we don't need to.
      return null
    }
  } else if (props.error) {
    // If we aren't loading, maybe
    return centered(<div>Error! Page failed to load</div>)
  } else {
    // This case shouldn't happen... but we'll return null anyways.
    return null
  }
}

export default Loading
