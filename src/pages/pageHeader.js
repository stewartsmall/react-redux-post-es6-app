import React from 'react'
import Toolbar from 'react-md/lib/Toolbars'
import Button from 'react-md/lib/Buttons/Button'
import { connect } from 'react-redux'
import { goToNew } from '../reducers/pageReducer'

function newPostClick(props) {
    if(props.new) {
        props.dispatch(props.goToNewPage())
    }
}

function homeLinkClick(props) {
    if(props.goHome) {
        props.dispatch(props.goHome())
    }
}

export class PageHeader extends React.Component {
    render() {
        const { props } = this       
        return (
            <Toolbar
                colored
                nav={<Button key="home" onClick={() => homeLinkClick(props)} icon>home</Button>}
                actions={<Button key="add" onClick={() => newPostClick(props)} icon>add</Button>}
                title={props.title}
            />
        )
    }
}

const mapStateToProps = state => ({
    goToNewPage: goToNew
})

export default connect(mapStateToProps)(PageHeader)