import React from 'react'
import { connect } from 'react-redux'
import { goToHome, goToEdit } from '../reducers/pageReducer'
import postsApi from '../api/postsApi'
import PageHeader from './pageHeader'

import Button from 'react-md/lib/Buttons/Button'
import Dialog from 'react-md/lib/Dialogs'
import Card from 'react-md/lib/Cards/Card'
import CardTitle from 'react-md/lib/Cards/CardTitle'
import Media, { MediaOverlay } from 'react-md/lib/Media'

export class PostDetail extends React.Component {
    constructor(props) {
        super(props)

        this.state = { visible: false, image: '' };
        this.handleOnClick = this.handleOnClick.bind(this)
        this.handleGoToEditClick = this.handleGoToEditClick.bind(this)
    }

    openDialog = () => {
        this.setState({ visible: true });
    };

    closeDialog = () => {
        this.setState({ visible: false });
    };
    

    async handleOnClick() {  
        const { props } = this
        const res = await postsApi.delete('/'+props.post.id)
        if(res.ok && res.data) {
            props.dispatch(props.goToHomePage())
        }  
    }

    handleGoToEditClick(id) {
        const { props } = this
        props.dispatch(this.props.goToEditPage(id))
    }

    render() {
        const { visible } = this.state
        const { title, categories, content, id, image } = this.props.post
        const picture = `http://localhost:8080/upload/?filename=${image}`
        return (
            <section>

                <PageHeader title="Post Details" goHome={this.props.goToHomePage} new={true} />

                <br />

                <Card style={{ maxWidth: '90%' }} className="md-block-centered">
                    <Media>
                    <img src={picture} alt="" />
                    <MediaOverlay>
                        <CardTitle title={title} subtitle={categories}>                        
                        </CardTitle>
                    </MediaOverlay>
                    </Media>
                    <div className="md-grid">        
                        <div className="md-cell md-cell--12"> 
                            <div>{content}</div>                        
                        </div>
                    </div>

                    <div className="md-grid">
                        <div className="md-cell md-cell--12">                        
                            <Button className="md-cell--right" raised primary onClick={() => this.handleGoToEditClick(id)}>Edit</Button>
                            <Button style={{ textAlign: 'right', float: 'right' }} className="md-cell--right" raised secondary onClick={this.openDialog}>Delete</Button>
                        </div>
                    </div>
                </Card>

                <Dialog
                    id="simpleDialogExample"
                    visible={visible}
                    title="Confirm Delete"
                    onHide={this.closeDialog}
                >     
                    <div className="md-grid">
                        <div className="md-cell md-cell--12">               
                            <Button className="md-cell--left" raised secondary onClick={this.handleOnClick}>Yes</Button>
                            <Button style={{ float: 'right' }} raised primary onClick={this.closeDialog}>No</Button>
                        </div>
                    </div>
                </Dialog>
                        
            </section>

        )
    }    
}


function mapStateToProps (state) {
    return {
        post: state.post,   
        goToHomePage: goToHome,
        goToEditPage: goToEdit
    }
}

export default connect(mapStateToProps)(PostDetail)