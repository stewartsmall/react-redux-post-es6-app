import React from 'react'
import { connect } from 'react-redux'
import Truncate from 'react-truncate'
import { goToPost, goToNew, goToEdit } from '../reducers/pageReducer'
import Link from 'redux-first-router-link'

import FontIcon from 'react-md/lib/FontIcons'
import Divider from 'react-md/lib/Dividers';
import Card from 'react-md/lib/Cards/Card'

import PageHeader from './pageHeader'

const ViewIcon = () => <FontIcon>visibility</FontIcon>
const EditIcon = () => <FontIcon>edit</FontIcon>

export const PostList = props =>
    <section>

        <PageHeader title="Post List" new={true} />

        <br />

        <Card style={{ maxWidth: '90%' }} className="md-block-centered"> 
                            
            {Object.keys(props.posts).map((id, index) => 
                <div key={id}>
                    <br />
                    <div className="cmd-grid">
                        <div className="md-cell md-cell--12">   
                            <div key={id}>  
                                <h3>{props.posts[id].title}</h3>
                                <div>
                                    <Truncate lines={1}>{props.posts[id].content}</Truncate>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <br />
                    <div className="cmd-grid">
                        <div className="md-cell md-cell--12">
                            <Link to={props.goToPostPage(id)}><ViewIcon /> View Post </Link>
                            <Link style={{ textAlign: 'right', float: 'right' }} to={props.goToEditPage(id)}> <EditIcon /> Edit Post</Link>
                        </div>
                        <Divider />
                    </div>
                </div>,
            )}  

        </Card>
        <br />
        <br/> 
    </section>

const mapStateToProps = state => ({
    posts: state.postsMap,
    goToPostPage: goToPost,
    goToNewPage: goToNew,
    goToEditPage: goToEdit,
    post: state.post
})

export default connect(mapStateToProps)(PostList)