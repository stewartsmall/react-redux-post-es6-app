import React from 'react'
import Form from 'react-formal'
import yup from 'yup'
import { goToHome } from '../reducers/pageReducer'
import { connect } from 'react-redux'
import Link from 'redux-first-router-link'
import postsApi from '../api/postsApi'
import uploadApi from '../api/uploadApi'
import PageHeader from './pageHeader'
import Snackbar from 'react-md/lib/Snackbars'

import Card from 'react-md/lib/Cards/Card'
import Button from 'react-md/lib/Buttons/Button'
import TextField from 'react-md/lib/TextFields'
import FileInput from 'react-md/lib/FileInputs'

const formPostSchema = yup.object().shape({
    title: yup.string().required('Please enter a title'),
    content: yup.string().required('Please enter some content this field is required'),
    categories: yup.string().required('Please enter some categories this field is required'),
    image: yup.string().required('Please select an image for this post'),
})

export class PostNewEdit extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            errors: {},
            toasts: [],
            fileName: '',
            pic: {}
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleErrors = this.handleErrors.bind(this)
        this._handleFileSelect = this._handleFileSelect.bind(this)
        this.handleInvalidSubmit = this.handleInvalidSubmit.bind(this)
    }

    _dismiss = () => {
        const toasts = this.state.toasts.slice();
        toasts.shift();
        this.setState({ toasts })
    };

    _handleOnFileChange = (file, event) => {
        const reader = new FileReader()
        const upFile = event.target.files[0]
        
        reader.onload = (upload) => {
            this.setState({
                fileName: upFile.name,
                pic: upFile
            }, () => this._handleFileSelect());
        }
        
        reader.readAsDataURL(upFile);        
    }
    
    _handleFileSelect = () => {
        const toasts = this.state.toasts.slice()
        const { fileName } = this.state

        if (!fileName) {
            toasts.push({ text: 'You did not select new file.' })
        } else {
            toasts.push({ text: `${fileName} has been selected.` })            
        }
        this.setState({ toasts })
    }

    handleInvalidSubmit(values){
        const toasts = this.state.toasts.slice()
        if(values.image[0].message) {
            toasts.push({ text: `${values.image[0].message}` })
        }
        this.setState({ toasts })
    }

    async handleSubmit(values){ 
        let { post } = this.props
        let { fileName, pic } = this.state
        let { props } = this
        var formData = new FormData()
        formData.append("image", pic) 
        
        if(post && !fileName && post.image !== '') {
            values.image = post.image
        } else {
            values.image = this.state.fileName
        }
        
        if(post) {
            const res = await postsApi.put(`/${post.id}`, values)                        
            if(res.ok && res.data) {
                const resUpload = await uploadApi.post('/', formData)
                if(resUpload.ok && resUpload.data) {
                    props.dispatch(props.goToHomePage())
                } else {
                    console.log("Failed to upload image: check if sever is running")
                } 
            } 
        } else {            
            const res = await postsApi.post('/', values)
            const resUpload = await uploadApi.post('/', formData)
            if(res.ok && res.data && resUpload.ok && resUpload.data) {
                props.dispatch(props.goToHomePage())
            } 
        }
    }

    handleErrors = errors => {
        this.setState({errors})        
    }

    render() {
        const errors = this.state.errors
        const { post } = this.props
        let headerTitle = (post) ? "Edit Post" : "Create Post"
        const { toasts } = this.state

        let title = (post) ? post.title : ""
        let categories = (post) ? post.categories : ""
        let content = (post) ? post.content : ""
        let image = (post) ? post.image : ""      

        return(
            <section>   

                <PageHeader title={headerTitle} goHome={this.props.goToHomePage} />

                <br />

                <Card style={{ maxWidth: 1000 }} className="md-block-centered">
                    <Form
                    schema={formPostSchema}
                    defaultValue={{
                        title: title,
                        categories: categories,
                        content: content,
                        image: image
                    }}
                    onSubmit={this.handleSubmit}
                    onInvalidSubmit={this.handleInvalidSubmit}
                    errors={errors}
                    onError={this.handleErrors}
                    >
                        <div className="md-grid">

                            <Form.Field
                                name="image"
                                type={FileInput}
                                label="Image"
                                id="videoFile"
                                onChange={this._handleOnFileChange}
                                noMeta
                            />

                            <Form.Field
                                name="title"                                
                                type={TextField}
                                label="Title"
                                id="floatingCenterTitle"
                                noMeta
                                error={!!errors.title}
                                errorText={errors.title && errors.title[0].message}
                            />

                            <Form.Field
                                name="categories"
                                type={TextField}
                                label="Categories"
                                id="floatingCenterTitlee"
                                noMeta
                                error={!!errors.categories}
                                errorText={errors.categories && errors.categories[0].message}
                            />

                            <Form.Field
                                name="content"
                                type={TextField}                                
                                label="Content"
                                rows={5}
                                id="floatingMultiline"
                                noMeta
                                error={!!errors.content}
                                errorText={errors.content && errors.content[0].message}
                            />

                            <Button raised primary children="Submit" type="submit" className="md-cell--left" />

                            <Link to={this.props.goToHomePage()} className="md-cell--right">
                                Cancel
                            </Link>

                        </div>

                    </Form>
                </Card>                
                <Snackbar toasts={toasts} onDismiss={this._dismiss} />
            </section>
        )
    }
   
}

function mapStateToProps(state = null) {
    return {
        goToHomePage: goToHome,
        post: state.post
    }
}

export default connect(mapStateToProps)(PostNewEdit)