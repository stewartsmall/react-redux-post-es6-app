import React from 'react'

const NotFound = () =>
    <section>
        <div className="container">
            <div className="Row">
                <div className="col-md-12">
                    <h2>Not Found</h2>
                </div>
            </div>
        </div>
    </section>

export default NotFound