import { createStore, applyMiddleware, combineReducers, compose} from 'redux'
import { connectRoutes } from 'redux-first-router'
import loggerMiddleware from 'redux-logger'
import createHistory from 'history/createBrowserHistory'

import routesMap from './routesMap'
import * as reducers from '../reducers'

export default function configureStore() {
    const history = createHistory()

    const {
        reducer: routeReducer,
        middleware: routeMiddleware,
        enhancer: routeEnhancer
    } = connectRoutes(history, routesMap)

    const rootReducer = combineReducers({
        ...reducers,
        location: routeReducer
    })

    const middleware = applyMiddleware(routeMiddleware, loggerMiddleware)
    const enhancer = composeEnhancers(routeEnhancer, middleware)

    return createStore(rootReducer, enhancer)
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose