import { NOT_FOUND } from 'redux-first-router'
import { GO_TO_HOME, GO_TO_POST, GO_TO_NEW_POST, GO_TO_EDIT_POST } from '../reducers/pageReducer'
import { postsFetched } from '../reducers/postsReducer'
import { fetchPost } from '../reducers/postReducer'
import postsApi from '../api/postsApi'

const routesMap = {
    [GO_TO_HOME]: {
        path: '/',
        thunk: async (dispatch, getState) => {
            const response = await postsApi.get('')
            if (response.ok && response.data) {
                dispatch(postsFetched(response.data))
            } else {
                dispatch({ type: NOT_FOUND })
            }
        }
    },
    [GO_TO_POST]: {
        path: '/posts/:id',
        thunk: async (dispatch, getState) => {
            const { location } = getState()
            const response = await postsApi.get(''+location.payload.id)
            if(response.ok && response.data) {
                dispatch(fetchPost(response.data))
            } else {
                dispatch({ type: NOT_FOUND })
            }
        }
    },
    [GO_TO_EDIT_POST]: {
        path: '/posts/edit/:id',
        thunk: async (dispatch, getState) => {
            const { location } = getState()
            const response = await postsApi.get(''+location.payload.id)
            if(response.ok && response.data) {
                dispatch(fetchPost(response.data))
            } else {
                dispatch({ type: NOT_FOUND })
            }
        }
    },
    [GO_TO_NEW_POST]: {
        path: '/post/new'
    }
}

export default routesMap