import * as React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import App from './root/App'
import configureStore from './store/configuredStore'
import registerServiceWorker from './startup/registerServiceWorker'

const store = configureStore()

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
)

registerServiceWorker()