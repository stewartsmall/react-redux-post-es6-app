import { createSelector } from 'reselect'
import { GO_TO_HOME, GO_TO_POST, GO_TO_EDIT_POST } from '../reducers/pageReducer'

export default createSelector(
  [state => state.location.type, state => state.post, state => state.postsMap],
  (type, post, postsMap) => {      
    if (type === GO_TO_HOME) {
      return !postsMap
    }
    if (type === GO_TO_POST) {
      return !post
    }
    if (type === GO_TO_EDIT_POST) {
      return !post
    }
    return false
  },
)
