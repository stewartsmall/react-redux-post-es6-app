/* eslint-disable import/no-extraneous-dependencies, import/no-unresolved, import/extensions */

import { configure } from '@storybook/react'
import { injectGlobal } from 'styled-components'

import '../src/index.css'

injectGlobal`
  #root {
    height: 100%
  }
`

import '../src/startup/loadWebFont.ts'

const req = require.context('../', true, /story\.tsx$/)

function loadStories() {
  req.keys().forEach(req)
}

configure(loadStories, module)
